echo -e "\nGit 4 dummies\nIstruzioni \n"
echo -e "Ciao e benvenuto nell'esercitazione collettiva di git 4 dummies.\nPrima di iniziare ogni esercizio, è bene che tu sappia come consegnare le soluzioni di alcuni esercizi.\nAl termine di essi, infatti, le discuteremo tutti insieme"
echo -e "\nSe questa repo è il fork della repo di esercitazione, puoi iniziare. Altrimenti, chiedici come fare!"
echo -e "\nScrivi nel terminale:\n\n       sh primoEs.sh\n\nBuon lavoro :)"
