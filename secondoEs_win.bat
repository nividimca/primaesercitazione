ECHO OFF
chcp 65001
CLS
MKDIR Es2
type NUL > Es2/first.txt
type NUL > Es2/second.txt
type NUL > Es2/third.txt
type NUL > Es2/important.txt
git add Es2/first.txt Es2/third.txt Es2/important.txt
git commit -m "add first.txt third.txt and important.txt in Es2/"
ECHO new changes > Es2/third.txt
ECHO Important changes!! > Es2/important.txt
git add Es2/third.txt Es2/important.txt
ECHO.
ECHO.
ECHO Git 4 dummies
ECHO Secondo esercizio
ECHO.
ECHO.
ECHO Hai creato e committato un po' di file nella cartella Es2/ (dacci un occhio).
ECHO Infatti, il tuo ultimo commit è:"
git log -1
ECHO.
ECHO.
ECHO Ora però vuoi eliminare tutti i file (anche dal versionamento) in "Es2/" ESCLUSO "Es2/important.txt", che dovrà essere solo rimosso dal versionamento.
ECHO Questi file non voglio vederli mai più, nemmeno nei prossimi commit. Dovresti pensare ad un modo per farlo.
ECHO.
ECHO Buon lavoro :)
ECHO.
ECHO Hint: prima di agire, guarda cosa c'è nella staging area!
